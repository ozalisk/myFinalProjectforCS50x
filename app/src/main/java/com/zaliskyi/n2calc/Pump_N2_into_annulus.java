package com.zaliskyi.n2calc;

import android.graphics.Color;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class Pump_N2_into_annulus extends AppCompatActivity {


    Button button_casing_ok;
    Button calculate_button;
    Double nitrogen_m3;
    HCWell well;
    EditText bashmak_edittext;
    EditText desired_level_edittext;
    Boolean bashmak_isvalid = true;
    Boolean des_valid = true;
    CheckBox bashmak_checkbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        well = new HCWell(0.120,3000.0,0.073,0.055);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pump__n2_into_annulus);

        Integer l = TubingSize.tubing_collection.length;
        String[] mas = new String[l];
        for(int i = 0;i<l;i++){
            mas[i] = TubingSize.tubing_collection[i].toString();
        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,mas);
        final Spinner spinner = (Spinner)findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
        spinner.post(new Runnable() {
            @Override
            public void run() {
                spinner.setSelection(24);
            }
        });

        final Spinner casing_id_spinner;
        casing_id_spinner = (Spinner) findViewById(R.id.casingid_spinner);
        String[] sts = new String[401];
        for (int i = 0;i<401;i++)
        {
            double n = 100.0 + i*0.5;
            sts[i] = String.format("%.2f мм",n);
        }
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,sts);
        casing_id_spinner.setAdapter(adapter1);
        casing_id_spinner.post(new Runnable() {
            @Override
            public void run() {
                casing_id_spinner.setSelection(37);
            }
        });

        bashmak_edittext = (EditText) findViewById(R.id.bashmak_edittext);
        bashmak_edittext.setText("3000");
        bashmak_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {

                    Double bashmak = Double.valueOf(bashmak_edittext.getText().toString());
                    Double maxlevel = well.get_max_safe_level();
                    System.out.println("maxlevel = " + maxlevel.toString());
                    if((bashmak<0)||(bashmak>10000))
                    {
                        bashmak_edittext.setTextColor(Color.RED);
                        bashmak_isvalid = false;
                    }
                    else
                    {
                        bashmak_edittext.setTextColor(Color.BLACK);
                        well.tubing_length = bashmak;
                        bashmak_isvalid = true;
                    }

                    desired_level_edittext = (EditText) findViewById(R.id.desired_level_edittext);
                    Double desired = Double.valueOf(desired_level_edittext.getText().toString());
                    if((desired>well.get_max_safe_level()))
                    {
                        desired_level_edittext.setTextColor(Color.RED);
                        des_valid = false;
                    }
                    else
                    {
                        desired_level_edittext.setTextColor(Color.BLACK);
                        des_valid = true;
                    }

                }
                catch (NumberFormatException ex) { }

            }
        });


        desired_level_edittext = (EditText) findViewById(R.id.desired_level_edittext);
        desired_level_edittext.setText("2000");
        desired_level_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                TextView screenmessage = (TextView) findViewById(R.id.screenmessage_textview);

                try {

                    Double desired = Double.valueOf(desired_level_edittext.getText().toString());
                    if(!((desired>0)&&(desired<well.get_max_safe_level())))
                    {
                        desired_level_edittext.setTextColor(Color.RED);
                        des_valid = false;
                        screenmessage.setText("Занадто глибоко бажаний рівень! Буде прорив азоту в башмак НКТ");
                    }
                    else
                    {
                        desired_level_edittext.setTextColor(Color.BLACK);
                        des_valid = true;
                        screenmessage.setText("");
                    }
                }
                catch (NumberFormatException ex) { }

            }
        });

        //Create spinner and populate it
        final Spinner density_spinner = (Spinner) findViewById(R.id.killfluid_density_spinner);
        String[] density_family = new String[101];
        for(int i = 0,dens = 500; i<density_family.length;i++,dens+=10)
        {
            density_family[i] = String.format("%d кг/м3",600+i*10);
        }
        ArrayAdapter<String> dens_adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,density_family);
        density_spinner.setAdapter(dens_adapter);
        density_spinner.post(new Runnable() {
            @Override
            public void run() {
                density_spinner.setSelection(46);
            }
        });

        bashmak_checkbox = (CheckBox) findViewById(R.id.bashmak_checkbox);
        bashmak_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                TextView which_level = (TextView) findViewById(R.id.whichlevel_textbox);
                View level_layout = (View) findViewById(R.id.desired_level_layout);
                if (bashmak_checkbox.isChecked())
                {
                    which_level.setText("ОК, віддуваємо свердловину до башмака насухо");
                    desired_level_edittext.setFocusable(false);
                    level_layout.setVisibility(View.INVISIBLE);
                    des_valid = true;
                }
                else
                {
                    des_valid = false;
                    which_level.setText("Який рівень хочете після стравлення азоту?");
                    desired_level_edittext.setFocusable(true);
                    desired_level_edittext.setText("");
                    level_layout.setVisibility(View.VISIBLE);
                    desired_level_edittext.setFocusableInTouchMode(true);
                    //desired_level_edittext.requestFocus();
                }
            }
        });

        calculate_button = (Button) findViewById(R.id.calculate_button);
        calculate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Якщо дані про глибину башмака і необхідний рівень введені валідні, то колір чорний,
                // а не червоний

                try {
                    if (bashmak_isvalid && des_valid) {
                        //Визначення типорозміру експлуатаційної колони по позиції спіннера
                        int csg_spn_position = casing_id_spinner.getSelectedItemPosition();
                        well.casing_id = 0.1 +0.0005*csg_spn_position;
                        System.out.println("CASing ID = "+ well.casing_id +" m");

                        //Визначення типорозміру НКТ по позиції спіннера
                        int tbg_selected = spinner.getSelectedItemPosition();
                        well.tubing_od = TubingSize.tubing_collection[tbg_selected].od;
                        well.tubing_wall = TubingSize.tubing_collection[tbg_selected].wall;
                        System.out.println("Tubing: "+well.tubing_od+" x "+well.tubing_wall);

                        //Визначення довжини НКТ по текстбоксу
                        well.tubing_length = Double.valueOf(bashmak_edittext.getText().toString());

                        //Перерахунок площ
                        well.recalculate_areas();
                        System.out.println("tubing shoe at:" + well.tubing_length);

                        well.kill_fluid_density = 600.0 + 10 * density_spinner.getSelectedItemPosition();


                        Double deslev;
                                if(bashmak_checkbox.isChecked()) {
                                    deslev = well.get_max_safe_level();
                                }
                                else {
                                    deslev = Double.valueOf(desired_level_edittext.getText().toString());
                                }
                        //System.out.println("desired level = "+ deslev.toString()+" м");
                        //System.out.println("RO = "+ well.kill_fluid_density);
                        System.out.println("MAXLEV = "+well.get_max_safe_level());
                        Double[] n2 = well.displace_N2(deslev);


                        //return new Double[]{0.0,nitrogen_m3,mirror_achieved_m};
                        TextView screenmessage = (TextView) findViewById(R.id.screenmessage_textview);
                        screenmessage.setText(String.format("Обʼєм закачаного в затрубний простір азоту: %.0f м3. \n " +
                                "Досягнута азотною подушкою глибина: %.2f м \n" +
                                "Тиск в затрубі в кінці закачки азоту: %.3f МПа", n2[1], n2[2],n2[3]));
                    }
                }
                catch (NumberFormatException ex)
                {
                    TextView messagescreen_edittext = (TextView) findViewById(R.id.screenmessage_textview);
                    messagescreen_edittext.setText("Рівень не введено!");
                };
            }
        });







    }


}
