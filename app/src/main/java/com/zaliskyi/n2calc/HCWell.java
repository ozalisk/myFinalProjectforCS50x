package com.zaliskyi.n2calc;

/**
 * Created by oleksandr on 25.08.17.
 */

public class HCWell {
    //Fields
    public static final Double G = 9.81;
    //Geometric properties of tubing

    public Double tubing_length;       //Length of tubing string,m
    public Double tubing_od;           //OD of tubing string,m
    public Double tubing_wall;         //tubing_wall,m
    Double tubing_id;           //ID of tubing, m
    Double tubing_inner_crs;    //inner cross section of tubing
    Double tubing_metal_disp;   //metal cross section
    Double annulus_crs;


    //Geometric properties of casing
    public Double casing_id;
    public Double casing_inner_crs;

    //Kill fluid SG
    public Double kill_fluid_density = 1050.0;

    //Geothermal gradient
    Double geothermal_gradient = 0.02;

    //Constructors
    public HCWell(Double csg_id, Double tbg_length, Double tbg_od, Double tbg_wall ){
        casing_id = csg_id;
        tubing_length = tbg_length;
        tubing_od = tbg_od;
        tubing_wall = tbg_wall;

        //Calculate the rest of properties
        recalculate_areas();


    }

    //Methods
    //returns Nitrogen mass in kgs
    public Double[] displace_N2(double desired_level){

        Double[] result;

        //Calculate annular_volume
        Double annulus_volume = tubing_length*(casing_inner_crs-tubing_metal_disp-tubing_inner_crs);


        //Check if we can lower the level without nitrogen going into the tubing string
        //if there is a risk of nitrogen going into the tubing shoe. an array with -1 at the start returned in this case

        Double displaced_volume = desired_level*(tubing_inner_crs+annulus_crs);


        if (displaced_volume > annulus_volume*1.001){
            System.out.println("DEBUG 21\n\n\n");
            result = new Double[]{-1.0, 0.0, 0.0};
            return result;
        }

        //Double mirror_achieved_m = displaced_volume/(casing_inner_crs-tubing_metal_disp-tubing_inner_crs);
        Double mirror_achieved_m = displaced_volume/(annulus_crs);

        //Pressure on the mirror
        Double hydrostaticPressure = mirror_achieved_m*G*kill_fluid_density;
        Double pressure_abs_mirror = hydrostaticPressure+101325;

        //Temperature on the depth of mirror
        Double mirror_temperature = 291.15+0.02*mirror_achieved_m;
        Double temperature_average = (291.15 + mirror_temperature)*0.5;
        Double exp = 0.9669*0.03415*mirror_achieved_m*2/(291.15+mirror_temperature);
        Double surf_pressure_abs = pressure_abs_mirror/Math.exp(exp);
        Double surf_pressure_gauge = surf_pressure_abs - 101325;
        Double pressure_abs_average = 0.5*(surf_pressure_abs+pressure_abs_mirror);

        /*System.out.println("Mirror at " + mirror_achieved_m);
        System.out.println("exponent = " + exp.toString());
        System.out.println("Displaced volume, m3: "+displaced_volume.toString());
        System.out.println("hydrostatic pressure, Pa " + hydrostaticPressure);
        System.out.println("Surface pressure, Pa " + (surf_pressure_abs-101325));*/

        Double nitrogen_m3 = displaced_volume*(273.15/temperature_average)*pressure_abs_mirror/101325;
        Double nitrogen_kgs = 1.2506*nitrogen_m3;

        System.out.println("N2 m3: " + nitrogen_m3);
        System.out.println("N2 kgs: " + nitrogen_kgs);

        return new Double[]{0.0,nitrogen_m3,mirror_achieved_m,surf_pressure_gauge/1000000};
    }

    // A method to calculate the lowest level we can reach after nitrogen bleedoff
    public Double get_max_safe_level(){

        //Calculate annular_volume
        Double annulus_volume = tubing_length*(casing_inner_crs-tubing_metal_disp-tubing_inner_crs);

        //Check if we can lower the level without nitrogen going into the tubing string
        //if there is a risk of nitrogen going into the tubing shoe. an array with -1 at the start returned in this case

        return annulus_volume/(casing_inner_crs-tubing_metal_disp);

    }

    public void recalculate_areas(){
        //Calculate the rest of properties
        tubing_id = tubing_od - 2*tubing_wall;
        tubing_inner_crs = Math.PI*0.25*Math.pow(tubing_id,2);
        tubing_metal_disp = 0.25*Math.PI*(Math.pow(tubing_od,2))-tubing_inner_crs;
        casing_inner_crs = Math.PI*0.25*Math.pow(casing_id,2);
        annulus_crs = Math.PI*0.25*(casing_id*casing_id-tubing_od*tubing_od);
        System.out.println(String.format("tubing id = %f,Inner_tub_crs = %f, metal= %f, casing_inner_crs = %f",tubing_id ,tubing_inner_crs,tubing_metal_disp,casing_inner_crs));
    }
    @Override
    public String toString()
    {
        return String.format("Колона %f, НКТ %f x %f спущено до %f метрів.Заповнено рідиною глушіння %f кг/м3", casing_id,tubing_od,tubing_wall,tubing_length,kill_fluid_density);
    }
}
