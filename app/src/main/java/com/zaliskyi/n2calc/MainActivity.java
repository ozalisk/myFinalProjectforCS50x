package com.zaliskyi.n2calc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    Button pump_annulus_button;
    Button converter_button;
    Button svs_button;
    Button about_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        pump_annulus_button = (Button) findViewById(R.id.pump_annulus_button);
        converter_button =    (Button) findViewById(R.id.converter_button);
        about_button = (Button) findViewById(R.id.about_button);

        pump_annulus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),Pump_N2_into_annulus.class);
                startActivity(intent);
            }
        });

    }
}
