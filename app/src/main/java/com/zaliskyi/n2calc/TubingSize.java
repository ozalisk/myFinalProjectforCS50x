/**
 * Created by oleksandr on 25.08.17.
 */
package com.zaliskyi.n2calc;

public class TubingSize {

    public Double od;
    public Double wall;

    static  TubingSize[] tubing_collection = new TubingSize[]{

            new TubingSize(0.02667,0.00287),
            new TubingSize(0.02667,0.00391),

            new TubingSize(0.0334,0.00338),
            new TubingSize(0.0334,0.00455),

            new TubingSize(0.04216,0.00318),
            new TubingSize(0.04216,0.00356),
            new TubingSize(0.04216,0.00485),

            new TubingSize(0.04826,0.00318),
            new TubingSize(0.04826,0.00368),
            new TubingSize(0.04826,0.00506),
            new TubingSize(0.04826,0.00635),
            new TubingSize(0.04826,0.00762),

            new TubingSize(0.0508,0.00419),

            new TubingSize(0.0524,0.00396),
            new TubingSize(0.0524,0.00419),
            new TubingSize(0.0524,0.00572),

            new TubingSize(0.06032,0.00424),
            new TubingSize(0.06032,0.00483),
            new TubingSize(0.06032,0.00554),
            new TubingSize(0.06032,0.00645),
            new TubingSize(0.06032,0.00662),
            new TubingSize(0.06032,0.00712),
            new TubingSize(0.06032,0.00749),
            new TubingSize(0.06032,0.00853),

            new TubingSize(0.07302,0.00551),
            new TubingSize(0.07302,0.00701),
            new TubingSize(0.07302,0.00782),
            new TubingSize(0.07302,0.00803),
            new TubingSize(0.07302,0.00864),
            new TubingSize(0.07302,0.00919),
            new TubingSize(0.07302,0.01023),
            new TubingSize(0.07302,0.01118),

            new TubingSize(0.08890,0.00549),
            new TubingSize(0.08890,0.00645),
            new TubingSize(0.08890,0.00734),
            new TubingSize(0.08890,0.00935),
            new TubingSize(0.08890,0.00953),
            new TubingSize(0.08890,0.0105),
            new TubingSize(0.08890,0.01092),
            new TubingSize(0.08890,0.01143),
            new TubingSize(0.08890,0.01209),
            new TubingSize(0.08890,0.01240),
            new TubingSize(0.08890,0.01295),
            new TubingSize(0.08890,0.01346)/*,

            new TubingSize(0.1016,0.00574),
            new TubingSize(0.1016,0.00665),
            new TubingSize(0.1016,0.00838),
            new TubingSize(0.1016,0.01054),
            new TubingSize(0.1016,0.01270),
            new TubingSize(0.1016,0.01549),

            new TubingSize(0.1143,0.00688),
            new TubingSize(0.1143,0.00737),
            new TubingSize(0.1143,0.00856),
            new TubingSize(0.1143,0.0965),
            new TubingSize(0.1143,0.01092),
            new TubingSize(0.1143,0.0127),
            new TubingSize(0.1143,0.01422),
            new TubingSize(0.1143,0.016)*/
    };

    public TubingSize(Double OD, Double wall){
        od = OD;
        this.wall = wall;
    }

    @Override
    public String toString() {
        String odstr = String.format("%.2f", od*1000);
        String wstr = String.format("%.2f", wall*1000);

        String result = odstr + " мм x " + wstr+" мм";
        return result;
    }
}
